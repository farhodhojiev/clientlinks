﻿namespace Octopus.Extension.ClientLinks
{
    partial class ClientLinksInfo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView_guarator_for = new System.Windows.Forms.ListView();
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.link_guarator_for = new System.Windows.Forms.LinkLabel();
            this.panel_guarantor_for = new System.Windows.Forms.Panel();
            this.link_member_of = new System.Windows.Forms.LinkLabel();
            this.panel_member_of = new System.Windows.Forms.Panel();
            this.listView_member_of = new System.Windows.Forms.ListView();
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader28 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.link_solidary_group_loan = new System.Windows.Forms.LinkLabel();
            this.panel_solidary_group_loan = new System.Windows.Forms.Panel();
            this.listView_solidary_group_loan = new System.Windows.Forms.ListView();
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader22 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader23 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader24 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader25 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader26 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.link_collateral_owner = new System.Windows.Forms.LinkLabel();
            this.panel_collateral_owner = new System.Windows.Forms.Panel();
            this.listView_collateral_owner = new System.Windows.Forms.ListView();
            this.columnHeader27 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader29 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader30 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader31 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader32 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader33 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader34 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader35 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader36 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel_guarantor_for.SuspendLayout();
            this.panel_member_of.SuspendLayout();
            this.panel_solidary_group_loan.SuspendLayout();
            this.panel_collateral_owner.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView_guarator_for
            // 
            this.listView_guarator_for.AllowColumnReorder = true;
            this.listView_guarator_for.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader17,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader1,
            this.columnHeader9,
            this.columnHeader12});
            this.listView_guarator_for.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView_guarator_for.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listView_guarator_for.FullRowSelect = true;
            this.listView_guarator_for.GridLines = true;
            this.listView_guarator_for.LabelWrap = false;
            this.listView_guarator_for.Location = new System.Drawing.Point(0, 0);
            this.listView_guarator_for.MultiSelect = false;
            this.listView_guarator_for.Name = "listView_guarator_for";
            this.listView_guarator_for.Size = new System.Drawing.Size(960, 75);
            this.listView_guarator_for.TabIndex = 0;
            this.listView_guarator_for.UseCompatibleStateImageBehavior = false;
            this.listView_guarator_for.View = System.Windows.Forms.View.Details;
            this.listView_guarator_for.SelectedIndexChanged += new System.EventHandler(this.listView_guarator_for_SelectedIndexChanged);
            this.listView_guarator_for.DoubleClick += new System.EventHandler(this.listView_guarator_for_DoubleClick);
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "contract_id";
            this.columnHeader17.Width = 4;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Clent Name";
            this.columnHeader2.Width = 186;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Contract code";
            this.columnHeader3.Width = 132;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Status";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader4.Width = 61;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Amount";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader5.Width = 94;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Start date";
            this.columnHeader6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader6.Width = 79;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "End date";
            this.columnHeader7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader7.Width = 81;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Guaranteed amount";
            this.columnHeader8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader8.Width = 89;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "% of Loan";
            this.columnHeader1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "OLB";
            this.columnHeader9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Description";
            this.columnHeader12.Width = 93;
            // 
            // link_guarator_for
            // 
            this.link_guarator_for.AutoSize = true;
            this.link_guarator_for.Dock = System.Windows.Forms.DockStyle.Top;
            this.link_guarator_for.Location = new System.Drawing.Point(0, 0);
            this.link_guarator_for.Name = "link_guarator_for";
            this.link_guarator_for.Padding = new System.Windows.Forms.Padding(10, 7, 10, 2);
            this.link_guarator_for.Size = new System.Drawing.Size(98, 22);
            this.link_guarator_for.TabIndex = 1;
            this.link_guarator_for.TabStop = true;
            this.link_guarator_for.Text = "+ Guarantor for";
            this.link_guarator_for.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.link_guarator_for_LinkClicked);
            // 
            // panel_guarantor_for
            // 
            this.panel_guarantor_for.Controls.Add(this.listView_guarator_for);
            this.panel_guarantor_for.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_guarantor_for.Location = new System.Drawing.Point(0, 22);
            this.panel_guarantor_for.Name = "panel_guarantor_for";
            this.panel_guarantor_for.Size = new System.Drawing.Size(960, 75);
            this.panel_guarantor_for.TabIndex = 2;
            // 
            // link_member_of
            // 
            this.link_member_of.AutoSize = true;
            this.link_member_of.Dock = System.Windows.Forms.DockStyle.Top;
            this.link_member_of.Location = new System.Drawing.Point(0, 97);
            this.link_member_of.Name = "link_member_of";
            this.link_member_of.Padding = new System.Windows.Forms.Padding(10, 5, 10, 2);
            this.link_member_of.Size = new System.Drawing.Size(86, 20);
            this.link_member_of.TabIndex = 3;
            this.link_member_of.TabStop = true;
            this.link_member_of.Text = "+ Member of";
            this.link_member_of.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.link_member_of_LinkClicked);
            // 
            // panel_member_of
            // 
            this.panel_member_of.Controls.Add(this.listView_member_of);
            this.panel_member_of.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_member_of.Location = new System.Drawing.Point(0, 117);
            this.panel_member_of.Name = "panel_member_of";
            this.panel_member_of.Size = new System.Drawing.Size(960, 75);
            this.panel_member_of.TabIndex = 4;
            // 
            // listView_member_of
            // 
            this.listView_member_of.AllowColumnReorder = true;
            this.listView_member_of.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader18,
            this.columnHeader11,
            this.columnHeader10,
            this.columnHeader28,
            this.columnHeader13,
            this.columnHeader15,
            this.columnHeader16});
            this.listView_member_of.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView_member_of.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listView_member_of.FullRowSelect = true;
            this.listView_member_of.GridLines = true;
            this.listView_member_of.LabelWrap = false;
            this.listView_member_of.Location = new System.Drawing.Point(0, 0);
            this.listView_member_of.MultiSelect = false;
            this.listView_member_of.Name = "listView_member_of";
            this.listView_member_of.Size = new System.Drawing.Size(960, 75);
            this.listView_member_of.TabIndex = 0;
            this.listView_member_of.UseCompatibleStateImageBehavior = false;
            this.listView_member_of.View = System.Windows.Forms.View.Details;
            this.listView_member_of.DoubleClick += new System.EventHandler(this.listView_member_of_DoubleClick);
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "group_id";
            this.columnHeader18.Width = 4;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Name";
            this.columnHeader11.Width = 197;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Type of Group";
            this.columnHeader10.Width = 120;
            // 
            // columnHeader28
            // 
            this.columnHeader28.Text = "Active";
            this.columnHeader28.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Establishment date";
            this.columnHeader13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader13.Width = 159;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "Joined date";
            this.columnHeader15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader15.Width = 168;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "Left date";
            this.columnHeader16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader16.Width = 162;
            // 
            // link_solidary_group_loan
            // 
            this.link_solidary_group_loan.AutoSize = true;
            this.link_solidary_group_loan.Dock = System.Windows.Forms.DockStyle.Top;
            this.link_solidary_group_loan.Location = new System.Drawing.Point(0, 192);
            this.link_solidary_group_loan.Name = "link_solidary_group_loan";
            this.link_solidary_group_loan.Padding = new System.Windows.Forms.Padding(10, 5, 10, 2);
            this.link_solidary_group_loan.Size = new System.Drawing.Size(126, 20);
            this.link_solidary_group_loan.TabIndex = 5;
            this.link_solidary_group_loan.TabStop = true;
            this.link_solidary_group_loan.Text = "+ Solidary group loan";
            this.link_solidary_group_loan.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.link_solidary_group_loan_LinkClicked);
            // 
            // panel_solidary_group_loan
            // 
            this.panel_solidary_group_loan.Controls.Add(this.listView_solidary_group_loan);
            this.panel_solidary_group_loan.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_solidary_group_loan.Location = new System.Drawing.Point(0, 212);
            this.panel_solidary_group_loan.Name = "panel_solidary_group_loan";
            this.panel_solidary_group_loan.Size = new System.Drawing.Size(960, 75);
            this.panel_solidary_group_loan.TabIndex = 6;
            // 
            // listView_solidary_group_loan
            // 
            this.listView_solidary_group_loan.AllowColumnReorder = true;
            this.listView_solidary_group_loan.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader19,
            this.columnHeader20,
            this.columnHeader21,
            this.columnHeader22,
            this.columnHeader23,
            this.columnHeader14,
            this.columnHeader24,
            this.columnHeader25,
            this.columnHeader26});
            this.listView_solidary_group_loan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView_solidary_group_loan.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listView_solidary_group_loan.FullRowSelect = true;
            this.listView_solidary_group_loan.GridLines = true;
            this.listView_solidary_group_loan.LabelWrap = false;
            this.listView_solidary_group_loan.Location = new System.Drawing.Point(0, 0);
            this.listView_solidary_group_loan.MultiSelect = false;
            this.listView_solidary_group_loan.Name = "listView_solidary_group_loan";
            this.listView_solidary_group_loan.Size = new System.Drawing.Size(960, 75);
            this.listView_solidary_group_loan.TabIndex = 0;
            this.listView_solidary_group_loan.UseCompatibleStateImageBehavior = false;
            this.listView_solidary_group_loan.View = System.Windows.Forms.View.Details;
            this.listView_solidary_group_loan.DoubleClick += new System.EventHandler(this.listView_solidary_group_loan_DoubleClick);
            // 
            // columnHeader19
            // 
            this.columnHeader19.Text = "contract_id";
            this.columnHeader19.Width = 7;
            // 
            // columnHeader20
            // 
            this.columnHeader20.Text = "Group Name";
            this.columnHeader20.Width = 145;
            // 
            // columnHeader21
            // 
            this.columnHeader21.Text = "Contract code";
            this.columnHeader21.Width = 164;
            // 
            // columnHeader22
            // 
            this.columnHeader22.Text = "Status";
            this.columnHeader22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader22.Width = 78;
            // 
            // columnHeader23
            // 
            this.columnHeader23.Text = "Amount";
            this.columnHeader23.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader23.Width = 75;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Share amount";
            this.columnHeader14.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader14.Width = 106;
            // 
            // columnHeader24
            // 
            this.columnHeader24.Text = "Start date";
            this.columnHeader24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader24.Width = 78;
            // 
            // columnHeader25
            // 
            this.columnHeader25.Text = "End date";
            this.columnHeader25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader25.Width = 77;
            // 
            // columnHeader26
            // 
            this.columnHeader26.Text = "Loan officer";
            this.columnHeader26.Width = 171;
            // 
            // link_collateral_owner
            // 
            this.link_collateral_owner.AutoSize = true;
            this.link_collateral_owner.Dock = System.Windows.Forms.DockStyle.Top;
            this.link_collateral_owner.Location = new System.Drawing.Point(0, 287);
            this.link_collateral_owner.Name = "link_collateral_owner";
            this.link_collateral_owner.Padding = new System.Windows.Forms.Padding(10, 5, 10, 2);
            this.link_collateral_owner.Size = new System.Drawing.Size(111, 20);
            this.link_collateral_owner.TabIndex = 7;
            this.link_collateral_owner.TabStop = true;
            this.link_collateral_owner.Text = "+ Collateral owner";
            this.link_collateral_owner.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.link_collateral_owner_LinkClicked);
            // 
            // panel_collateral_owner
            // 
            this.panel_collateral_owner.Controls.Add(this.listView_collateral_owner);
            this.panel_collateral_owner.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_collateral_owner.Location = new System.Drawing.Point(0, 307);
            this.panel_collateral_owner.Name = "panel_collateral_owner";
            this.panel_collateral_owner.Size = new System.Drawing.Size(960, 75);
            this.panel_collateral_owner.TabIndex = 8;
            // 
            // listView_collateral_owner
            // 
            this.listView_collateral_owner.AllowColumnReorder = true;
            this.listView_collateral_owner.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader27,
            this.columnHeader29,
            this.columnHeader30,
            this.columnHeader31,
            this.columnHeader32,
            this.columnHeader33,
            this.columnHeader34,
            this.columnHeader35,
            this.columnHeader36});
            this.listView_collateral_owner.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView_collateral_owner.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listView_collateral_owner.FullRowSelect = true;
            this.listView_collateral_owner.GridLines = true;
            this.listView_collateral_owner.LabelWrap = false;
            this.listView_collateral_owner.Location = new System.Drawing.Point(0, 0);
            this.listView_collateral_owner.MultiSelect = false;
            this.listView_collateral_owner.Name = "listView_collateral_owner";
            this.listView_collateral_owner.Size = new System.Drawing.Size(960, 75);
            this.listView_collateral_owner.TabIndex = 0;
            this.listView_collateral_owner.UseCompatibleStateImageBehavior = false;
            this.listView_collateral_owner.View = System.Windows.Forms.View.Details;
            this.listView_collateral_owner.DoubleClick += new System.EventHandler(this.listView_collateral_owner_DoubleClick);
            // 
            // columnHeader27
            // 
            this.columnHeader27.Text = "contract_id";
            this.columnHeader27.Width = 5;
            // 
            // columnHeader29
            // 
            this.columnHeader29.Text = "Clent Name";
            this.columnHeader29.Width = 145;
            // 
            // columnHeader30
            // 
            this.columnHeader30.Text = "Contract code";
            this.columnHeader30.Width = 164;
            // 
            // columnHeader31
            // 
            this.columnHeader31.Text = "Status";
            this.columnHeader31.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader31.Width = 78;
            // 
            // columnHeader32
            // 
            this.columnHeader32.Text = "Amount";
            this.columnHeader32.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader32.Width = 75;
            // 
            // columnHeader33
            // 
            this.columnHeader33.Text = "Start date";
            this.columnHeader33.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader33.Width = 77;
            // 
            // columnHeader34
            // 
            this.columnHeader34.Text = "End date";
            this.columnHeader34.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader34.Width = 75;
            // 
            // columnHeader35
            // 
            this.columnHeader35.Text = "Loan officer";
            this.columnHeader35.Width = 148;
            // 
            // columnHeader36
            // 
            this.columnHeader36.Text = "Collateral type";
            this.columnHeader36.Width = 121;
            // 
            // ClientLinksInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.panel_collateral_owner);
            this.Controls.Add(this.link_collateral_owner);
            this.Controls.Add(this.panel_solidary_group_loan);
            this.Controls.Add(this.link_solidary_group_loan);
            this.Controls.Add(this.panel_member_of);
            this.Controls.Add(this.link_member_of);
            this.Controls.Add(this.panel_guarantor_for);
            this.Controls.Add(this.link_guarator_for);
            this.Name = "ClientLinksInfo";
            this.Size = new System.Drawing.Size(960, 389);
            this.Load += new System.EventHandler(this.ClientLinksInfo_Load);
            this.panel_guarantor_for.ResumeLayout(false);
            this.panel_member_of.ResumeLayout(false);
            this.panel_solidary_group_loan.ResumeLayout(false);
            this.panel_collateral_owner.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        public System.Windows.Forms.ListView listView_guarator_for;
        private System.Windows.Forms.LinkLabel link_guarator_for;
        private System.Windows.Forms.Panel panel_guarantor_for;
        private System.Windows.Forms.LinkLabel link_member_of;
        private System.Windows.Forms.Panel panel_member_of;
        public System.Windows.Forms.ListView listView_member_of;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.LinkLabel link_solidary_group_loan;
        private System.Windows.Forms.Panel panel_solidary_group_loan;
        public System.Windows.Forms.ListView listView_solidary_group_loan;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.ColumnHeader columnHeader22;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private System.Windows.Forms.ColumnHeader columnHeader24;
        private System.Windows.Forms.ColumnHeader columnHeader25;
        private System.Windows.Forms.ColumnHeader columnHeader26;
        private System.Windows.Forms.LinkLabel link_collateral_owner;
        private System.Windows.Forms.Panel panel_collateral_owner;
        public System.Windows.Forms.ListView listView_collateral_owner;
        private System.Windows.Forms.ColumnHeader columnHeader29;
        private System.Windows.Forms.ColumnHeader columnHeader30;
        private System.Windows.Forms.ColumnHeader columnHeader31;
        private System.Windows.Forms.ColumnHeader columnHeader32;
        private System.Windows.Forms.ColumnHeader columnHeader33;
        private System.Windows.Forms.ColumnHeader columnHeader34;
        private System.Windows.Forms.ColumnHeader columnHeader35;
        private System.Windows.Forms.ColumnHeader columnHeader36;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private System.Windows.Forms.ColumnHeader columnHeader27;
        private System.Windows.Forms.ColumnHeader columnHeader28;

    }
}
