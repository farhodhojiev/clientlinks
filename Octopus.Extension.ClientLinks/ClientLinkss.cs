﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Octopus.Extensions;
using Octopus.CoreDomain;
using Octopus.CoreDomain.Clients;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Octopus.Extension.ClientLinks
{
    class ClientLinkss : IPerson
    {
        public TabPage[] GetTabPages(Person person)
        {
            var control = new ClientLinksInfo
            {
                Dock = DockStyle.Fill,
            };
            var tabPage = new TabPage("Client Links");

            //SELECT guarantor_for -------------------------------------
            //-------------------------------------------------------------------------
            string query = @"SELECT
            c.id AS id,
            COALESCE(corp.name, g.name, p.first_name + SPACE(1) + p.last_name+ SPACE(1)+ISNULL(p.father_name, '')) AS [client_name],
            c.contract_code AS contract_code,
            s.status_name AS status_name,
            cr.amount AS amount,
            CONVERT(NVARCHAR(10), c.[start_date], 120) AS [start_date],
            CONVERT(NVARCHAR(10), c.close_date, 120) AS close_date,
            lgc.guarantee_amount AS guarantee_amount,
            lgc.guarantee_amount/cr.amount*100 AS procent_of_cr,
            dbo.GetOLB(c.id, GETDATE()) AS OLB,
            ISNULL(lgc.guarantee_desc, '-') AS description
            FROM dbo.Contracts c
            LEFT JOIN dbo.Credit cr ON cr.id=c.id
            LEFT JOIN dbo.Statuses s ON s.id=c.status
            LEFT JOIN dbo.Tiers t ON t.id=c.tiers_id
            LEFT JOIN dbo.Persons p ON p.id=t.id
            LEFT JOIN dbo.Groups g ON g.id=t.id
            LEFT JOIN dbo.Corporates corp ON corp.id=t.id
            LEFT JOIN dbo.LinkGuarantorCredit lgc ON lgc.contract_id=c.id
            WHERE lgc.tiers_id=@client_id";

            using (var conn = DatabaseConnection.GetConnection())
            {
                var cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@client_id", person.Id);
                var reader = cmd.ExecuteReader();

                variables.i_guarantor_for = 0; //set 0, because global
                if (reader.HasRows) { variables.i_guarantor_for = 1; } //defining rows for hide or close this part

                while (reader.Read())
                {
                    Decimal id = reader.GetInt32(reader.GetOrdinal("id"));
                    String status = reader.GetString(reader.GetOrdinal("status_name"));
                    Decimal amount = reader.GetDecimal(reader.GetOrdinal("amount"));
                    Decimal guarantee_amount = reader.GetDecimal(reader.GetOrdinal("guarantee_amount"));
                    Decimal procent_of_cr = reader.GetDecimal(reader.GetOrdinal("procent_of_cr"));
                    Decimal OLB = reader.GetDecimal(reader.GetOrdinal("OLB"));
                    string[] row = {
                            id.ToString(),
                            reader.GetString(reader.GetOrdinal("client_name")),
                            reader.GetString(reader.GetOrdinal("contract_code")),
                            status,
                            amount.ToString(),
                            reader.GetString(reader.GetOrdinal("start_date")),
                            reader.GetString(reader.GetOrdinal("close_date")),
                            guarantee_amount.ToString(),
                            procent_of_cr.ToString(),
                            OLB.ToString(),
                            reader.GetString(reader.GetOrdinal("description")),
                            };
                    var listViewItemss = new ListViewItem(row);
                    if (status == "Active")
                    {
                        listViewItemss.Font = new Font(control.listView_guarator_for.Font, FontStyle.Bold);
                    }
                    control.listView_guarator_for.Items.Add(listViewItemss);
                }
            }

            //SELECT member_of -------------------------------------
            //-------------------------------------------------------------------------
            query = @"SELECT g.id AS id,
            g.name AS [client_name],
            'SolidarityGroup' AS group_type,
                        (CASE WHEN t2.active=1 THEN 'Yes'
		                ELSE 'No' END) AS [status],
            ISNULL(CONVERT(NVARCHAR(10), g.establishment_date, 120), '-') AS establishment_date,
            ISNULL(CONVERT(NVARCHAR(10), pgb.joined_date, 120), '-') AS joined_date,
            ISNULL(CONVERT(NVARCHAR(10), pgb.left_date, 120), '-') AS left_date
            FROM dbo.Tiers t
            LEFT JOIN dbo.Persons p ON p.id=t.id
            LEFT JOIN dbo.PersonGroupBelonging pgb ON pgb.person_id=p.id
            LEFT JOIN dbo.Groups g ON g.id=pgb.group_id
            LEFT JOIN dbo.Tiers t2 ON t2.id=g.id
            WHERE pgb.person_id=@client_id

            UNION ALL

            SELECT v.id AS id,
            v.name AS [client_name],
            'NonSolidarityGroup' AS group_type,
                        (CASE WHEN t2.active=1 THEN 'Yes'
		                ELSE 'No' END) AS [status],
            ISNULL(CONVERT(NVARCHAR(10), v.establishment_date, 120), '-') AS establishment_date,
            ISNULL(CONVERT(NVARCHAR(10), vp.joined_date, 120), '-') AS joined_date,
            ISNULL(CONVERT(NVARCHAR(10), vp.left_date, 120), '-') AS left_date
            FROM dbo.Tiers t
            LEFT JOIN dbo.Persons p ON p.id=t.id
            LEFT JOIN dbo.VillagesPersons vp ON vp.person_id=p.id
            LEFT JOIN dbo.Villages v ON v.id=vp.village_id
            LEFT JOIN dbo.Tiers t2 ON t2.id=v.id
            WHERE vp.person_id=@client_id

            UNION ALL

            SELECT corp.id AS id,
            corp.name AS [client_name],
            'Corporate' AS group_type,
                        (CASE WHEN t.active=1 THEN 'Yes'
		                ELSE 'No' END) AS [status],
            '-' AS establishment_date,
            '-' AS joined_date,
            '-' AS left_date
            FROM dbo.CorporatePersonBelonging cpb
            LEFT JOIN dbo.Persons p ON p.id=cpb.person_id
            LEFT JOIN dbo.Corporates corp ON corp.id=cpb.corporate_id
            LEFT JOIN dbo.Tiers t ON t.id=corp.id
            WHERE p.id=@client_id";

            using (var conn = DatabaseConnection.GetConnection())
            {
                var cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@client_id", person.Id);
                var reader = cmd.ExecuteReader();

                variables.i_member_of = 0; //set 0, because global
                if (reader.HasRows) { variables.i_member_of = 1; } //defining rows for hide or close this part

                while (reader.Read())
                {
                    Decimal id = reader.GetInt32(reader.GetOrdinal("id"));
                    String status = reader.GetString(reader.GetOrdinal("status"));
                    string[] row = {
                            id.ToString(),
                            reader.GetString(reader.GetOrdinal("client_name")),
                            reader.GetString(reader.GetOrdinal("group_type")),
                            status,
                            reader.GetString(reader.GetOrdinal("establishment_date")),
                            reader.GetString(reader.GetOrdinal("joined_date")),
                            reader.GetString(reader.GetOrdinal("left_date"))
                            };
                    var listViewItemss = new ListViewItem(row);
                    if (status == "Yes")
                    {
                        listViewItemss.Font = new Font(control.listView_member_of.Font, FontStyle.Bold);
                    }
                    control.listView_member_of.Items.Add(listViewItemss);
                }
            }

            //SELECT solidary_group_loan -------------------------------------
            //-------------------------------------------------------------------------
            query = @"SELECT c.id AS id,
                    g.name AS [client_name],
                    c.contract_code AS contract_code,
                    s.status_name AS status_name,
                    cr.amount AS amount,
                    lsa.amount AS lsa_amount,
                    CONVERT(NVARCHAR(10), c.[start_date], 120) AS [start_date],
                    CONVERT(NVARCHAR(10), c.close_date, 120) AS close_date,
                    u.last_name+SPACE(1)+u.first_name AS LO
                    FROM dbo.Contracts c
                    LEFT JOIN dbo.Credit cr ON cr.id=c.id
                    LEFT JOIN dbo.Statuses s ON s.id=c.status
                    LEFT JOIN dbo.Users u ON u.id=cr.loanofficer_id
                    LEFT JOIN dbo.Tiers t ON t.id=c.tiers_id
                    LEFT JOIN dbo.Persons p ON p.id=t.id
                    LEFT JOIN dbo.Groups g ON g.id=t.id
                    LEFT JOIN dbo.LoanShareAmounts lsa ON lsa.contract_id=c.id
                    WHERE lsa.person_id=@client_id";

            using (var conn = DatabaseConnection.GetConnection())
            {
                var cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@client_id", person.Id);
                var reader = cmd.ExecuteReader();

                variables.i_solidary_group_loan = 0; //set 0, because global
                if (reader.HasRows) { variables.i_solidary_group_loan = 1; } //defining rows for hide or close this part

                while (reader.Read())
                {
                    Decimal id = reader.GetInt32(reader.GetOrdinal("id"));
                    String status = reader.GetString(reader.GetOrdinal("status_name"));
                    Decimal amount = reader.GetDecimal(reader.GetOrdinal("amount"));
                    Decimal lsa_amount = reader.GetDecimal(reader.GetOrdinal("lsa_amount"));
                    string[] row = {
                            id.ToString(),
                            reader.GetString(reader.GetOrdinal("client_name")),
                            reader.GetString(reader.GetOrdinal("contract_code")),
                            status,
                            amount.ToString(),
                            lsa_amount.ToString(),
                            reader.GetString(reader.GetOrdinal("start_date")),
                            reader.GetString(reader.GetOrdinal("close_date")),
                            reader.GetString(reader.GetOrdinal("LO")),
                            //reader.GetString(reader.GetOrdinal("col_prod"))
                            };
                    var listViewItemss = new ListViewItem(row);
                    if (status == "Active")
                    {
                        listViewItemss.Font = new Font(control.listView_solidary_group_loan.Font, FontStyle.Bold);
                    }
                    control.listView_solidary_group_loan.Items.Add(listViewItemss);
                }
            }

            //SELECT collateral_owner -------------------------------------
            //-------------------------------------------------------------------------
            query = @"SELECT c.id AS id,
                    ISNULL(g.name, p.first_name + SPACE(1) + p.last_name+ SPACE(1)+ISNULL(p.father_name, '')) AS [client_name],
                    c.contract_code AS contract_code,
                    s.status_name AS status_name,
                    cr.amount AS amount,
                    CONVERT(NVARCHAR(10), c.[start_date], 120) AS [start_date],
                    CONVERT(NVARCHAR(10), c.close_date, 120) AS close_date,
                    u.last_name+SPACE(1)+u.first_name AS LO,
                    col_prod.name AS col_prod
                    FROM CollateralPropertyValues cpv
                    LEFT JOIN dbo.CollateralProperties cp ON cp.id=cpv.property_id
                    LEFT JOIN dbo.CollateralProducts col_prod ON col_prod.id=cp.product_id
                    LEFT JOIN dbo.CollateralPropertyTypes cpt ON cpt.id=cp.type_id
                    LEFT JOIN dbo.CollateralsLinkContracts clc ON clc.id=cpv.contract_collateral_id
                    LEFT JOIN dbo.Contracts c ON c.id=clc.contract_id
                    LEFT JOIN dbo.Credit cr ON cr.id=c.id
                    LEFT JOIN dbo.Statuses s ON s.id=c.status
                    LEFT JOIN dbo.Users u ON u.id=cr.loanofficer_id
                    LEFT JOIN dbo.Tiers t ON t.id=c.tiers_id
                    LEFT JOIN dbo.Persons p ON p.id=t.id
                    LEFT JOIN dbo.Groups g ON g.id=t.id
                    WHERE cpv.value=CAST(@client_id AS NVARCHAR(10)) AND cpt.name='Owner'";

            using (var conn = DatabaseConnection.GetConnection())
            {
                var cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@client_id", person.Id);
                var reader = cmd.ExecuteReader();

                variables.i_collateral_owner = 0; //set 0, because global
                if (reader.HasRows) { variables.i_collateral_owner = 1; } //defining rows for hide or close this part

                while (reader.Read())
                {
                    Decimal id = reader.GetInt32(reader.GetOrdinal("id"));
                    String status = reader.GetString(reader.GetOrdinal("status_name"));
                    Decimal amount = reader.GetDecimal(reader.GetOrdinal("amount"));
                    string[] row = {
                            id.ToString(),
                            reader.GetString(reader.GetOrdinal("client_name")),
                            reader.GetString(reader.GetOrdinal("contract_code")),
                            status,
                            amount.ToString(),
                            reader.GetString(reader.GetOrdinal("start_date")),
                            reader.GetString(reader.GetOrdinal("close_date")),
                            reader.GetString(reader.GetOrdinal("LO")),
                            reader.GetString(reader.GetOrdinal("col_prod"))
                            };
                    var listViewItemss = new ListViewItem(row);
                    if (status == "Active")
                    {
                        listViewItemss.Font = new Font(control.listView_collateral_owner.Font, FontStyle.Bold);
                    }
                    control.listView_collateral_owner.Items.Add(listViewItemss);
                }
            }

            tabPage.Controls.Add(control);
            return new[] { tabPage };
        }
        

        public void Save(Person person, System.Data.SqlClient.SqlTransaction tx)
        {
            //throw new NotImplementedException();
        }
    }
}
