﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using Octopus.Extensions;
using Octopus.CoreDomain;
using Octopus.CoreDomain.Clients;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.ComponentModel.Composition;

namespace Octopus.Extension.ClientLinks
{
    [Export(typeof(IExtension))]

    public class Extension : IExtension
    {
        //#region IExtension Members

        public string GetMeta(string key)
        {
            var meta = new Dictionary<string, string>
            {
                {"Name", "Client Links"},
                {"OctopusVersion", "v5.5.0"},
                {"Vendor", "Farhod Hojiev"},
                {"Version", "v1.0.0"}
            };
            return meta.ContainsKey(key) ? meta[key] : string.Empty;
        }

        public Guid Guid
        {
            get { return new Guid("72A58D47-E7F8-4AB8-AF79-766F730CB93B"); }
        }

        public object QueryInterface(Type t)
        {
            if (null == t) return null;
            var map = new Dictionary<Type, Type>
        {
            {typeof(IPerson), typeof(ClientLinkss)}
        };

            var targetType = map.ContainsKey(t) ? map[t] : null;
            if (null == targetType) return null;
            var ctor = targetType.GetConstructor(new Type[] { });
            return null == ctor ? null : ctor.Invoke(new object[] { });
        }
        //#endregion
    }
}
