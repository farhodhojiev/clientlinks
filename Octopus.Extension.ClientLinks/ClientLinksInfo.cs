﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Octopus.CoreDomain.Clients;
using Octopus.CoreDomain.Contracts.Loans;
using Octopus.GUI.Clients;
using Octopus.GUI;
using Octopus.Services;
using Octopus.Enums;

namespace Octopus.Extension.ClientLinks
{
    public partial class ClientLinksInfo : UserControl
    {
        public ClientLinksInfo()
        {
            InitializeComponent();
        }

        string lmax_guarantor_for = "- Guarantor for";
        string lmin_guarantor_for = "+ Guarantor for";
        string lmax_member_of = "- Member of";
        string lmin_member_of = "+ Member of";
        string lmax_solidary_group_loan = "- Solidary group loan";
        string lmin_solidary_group_loan = "+ Solidary group loan";
        string lmax_collateral_owner = "- Collateral owner";
        string lmin_collateral_owner = "+ Collateral owner";

        private void ClientLinksInfo_Load(object sender, EventArgs e)
        {
            if (variables.i_guarantor_for == 1)
            {
                link_guarator_for.Text = lmax_guarantor_for;
                panel_guarantor_for.Visible = true;
                variables.i_guarantor_for = 0;
            }
            else
            {
                link_guarator_for.Text = lmin_guarantor_for;
                panel_guarantor_for.Visible = false;
                variables.i_guarantor_for = 1;
            }
            //--
            if (variables.i_member_of == 1)
            {
                link_member_of.Text = lmax_member_of;
                panel_member_of.Visible = true;
                variables.i_member_of = 0;
            }
            else
            {
                link_member_of.Text = lmin_member_of;
                panel_member_of.Visible = false;
                variables.i_member_of = 1;
            }
            //--
            if (variables.i_solidary_group_loan == 1)
            {
                link_solidary_group_loan.Text = lmax_solidary_group_loan;
                panel_solidary_group_loan.Visible = true;
                variables.i_solidary_group_loan = 0;
            }
            else
            {
                link_solidary_group_loan.Text = lmin_solidary_group_loan;
                panel_solidary_group_loan.Visible = false;
                variables.i_solidary_group_loan = 1;
            }
            //--
            if (variables.i_collateral_owner == 1)
            {
                link_collateral_owner.Text = lmax_collateral_owner;
                panel_collateral_owner.Visible = true;
                variables.i_collateral_owner = 0;
            }
            else
            {
                link_collateral_owner.Text = lmin_collateral_owner;
                panel_collateral_owner.Visible = false;
                variables.i_collateral_owner = 1;
            }
        }

        private void link_guarator_for_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (variables.i_guarantor_for == 1)
            {
                link_guarator_for.Text = lmax_guarantor_for;
                panel_guarantor_for.Visible = true;
                variables.i_guarantor_for = 0;
            }
            else
            {
                link_guarator_for.Text = lmin_guarantor_for;
                panel_guarantor_for.Visible = false;
                variables.i_guarantor_for = 1;
            }
        }

        private void link_member_of_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (variables.i_member_of == 1)
            {
                link_member_of.Text = lmax_member_of;
                panel_member_of.Visible = true;
                variables.i_member_of = 0;
            }
            else
            {
                link_member_of.Text = lmin_member_of;
                panel_member_of.Visible = false;
                variables.i_member_of = 1;
            }
        }

        private void link_solidary_group_loan_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (variables.i_solidary_group_loan == 1)
            {
                link_solidary_group_loan.Text = lmax_solidary_group_loan;
                panel_solidary_group_loan.Visible = true;
                variables.i_solidary_group_loan = 0;
            }
            else
            {
                link_solidary_group_loan.Text = lmin_solidary_group_loan;
                panel_solidary_group_loan.Visible = false;
                variables.i_solidary_group_loan = 1;
            }
        }

        private void link_collateral_owner_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (variables.i_collateral_owner == 1)
            {
                link_collateral_owner.Text = lmax_collateral_owner;
                panel_collateral_owner.Visible = true;
                variables.i_collateral_owner = 0;
            }
            else
            {
                link_collateral_owner.Text = lmin_collateral_owner;
                panel_collateral_owner.Visible = false;
                variables.i_collateral_owner = 1;
            }
        }

        private void listView_guarator_for_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listView_guarator_for_DoubleClick(object sender, EventArgs e) {
            int contract_id = int.Parse(listView_guarator_for.SelectedItems[0].SubItems[0].Text);
            IClient client = ServicesProvider.GetInstance().GetClientServices().FindTiersByContractId(contract_id);
            var mainForm = (OctopusMainForm)Application.OpenForms[0];
            mainForm.InitializeCreditContractForm(client, contract_id);   
        }

        private void listView_member_of_DoubleClick(object sender, EventArgs e) {
            if ((listView_member_of.SelectedItems[0].SubItems[2].Text)=="SolidarityGroup"){
                int group_id = int.Parse(listView_member_of.SelectedItems[0].SubItems[0].Text);
                Group g = ServicesProvider.GetInstance().GetClientServices().FindGroupById(group_id);         
                var f = new ClientForm(g, ClientForm.ActiveForm);
                f.ShowDialog();
            }
            else if ((listView_member_of.SelectedItems[0].SubItems[2].Text)=="NonSolidarityGroup"){
                int nsgroup_id = int.Parse(listView_member_of.SelectedItems[0].SubItems[0].Text);
                Village g = (Village)ServicesProvider.GetInstance().GetClientServices().FindTiers(nsgroup_id, OClientTypes.Village);
                var f = new NonSolidaryGroupForm(g); 
                //frm.Show();
                //var mainForm = (LotrasmicMainWindowForm) Application.OpenForms[0];
                //mainForm.InitializeVillageForm((g));
                f.ShowDialog();
            }
            else if ((listView_member_of.SelectedItems[0].SubItems[2].Text)=="Corporate"){
                int corp_id = int.Parse(listView_member_of.SelectedItems[0].SubItems[0].Text);
                Corporate g = (Corporate)ServicesProvider.GetInstance().GetClientServices().FindTiers(corp_id, OClientTypes.Corporate);
                var f = new ClientForm(g, ClientForm.ActiveForm);
                f.ShowDialog();
            }
        }

        private void listView_solidary_group_loan_DoubleClick(object sender, EventArgs e)
        {
            int contract_id = int.Parse(listView_solidary_group_loan.SelectedItems[0].SubItems[0].Text);
            IClient client = ServicesProvider.GetInstance().GetClientServices().FindTiersByContractId(contract_id);
            var mainForm = (OctopusMainForm)Application.OpenForms[0];
            mainForm.InitializeCreditContractForm(client, contract_id);
        }

        private void listView_collateral_owner_DoubleClick(object sender, EventArgs e) 
        {
            int contract_id = int.Parse(listView_collateral_owner.SelectedItems[0].SubItems[0].Text);
            IClient client = ServicesProvider.GetInstance().GetClientServices().FindTiersByContractId(contract_id);
            var mainForm = (OctopusMainForm)Application.OpenForms[0];
            mainForm.InitializeCreditContractForm(client, contract_id);
        }

    }
}
